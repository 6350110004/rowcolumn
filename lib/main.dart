import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Row & Column',
      theme: ThemeData(

        primarySwatch: Colors.green,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: const Text('My row and Column'),
        ),
        body: Column(mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              child: Row(mainAxisAlignment: MainAxisAlignment.center,
                children:[
                  const CircleAvatar(
                    radius: 60,
                    backgroundImage: AssetImage('assets/imges/nat.jpg'),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(25.0),
                    child: Column(crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Hello',
                          style: TextStyle(
                            fontSize: 30,
                          ),
                        ),
                        Text('NaTcHaNoN',
                          style: TextStyle(
                            fontSize: 30,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

